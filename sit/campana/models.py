from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from campo.models import Land


class Campana(models.Model):
    recogida = models.DateField(_('recogida'))
    kilos = models.IntegerField(_('kilos'))
    dinero = models.FloatField(_('dinero'))
    campo = models.ForeignKey(to=Land, related_name="campanas",
                null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.campo.nombre

    def __unicode__(self):
        return self.campo.nombre