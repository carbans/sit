# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-12-29 10:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('campo', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campana',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('recogida', models.DateField(verbose_name='recogida')),
                ('kilos', models.IntegerField(verbose_name='kilos')),
                ('dinero', models.FloatField(verbose_name='dinero')),
                ('campo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='campanas', to='campo.Land')),
            ],
        ),
    ]
