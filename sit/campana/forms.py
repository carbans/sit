# -*- coding: utf-8 -*-
from django import forms

from .models import Campana
from campo.models import Land


class CampanaForm(forms.ModelForm):
    recogida = forms.DateField(required=True,
    widget=forms.TextInput(attrs={'class': 'form-control pull-rigth',
    'id': 'datepicker', 'placeholder': 'Ej. 01/01/2017'}))
    kilos = forms.IntegerField(required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 1500'}))
    dinero = forms.FloatField(required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 1200'}))
    campo = forms.ModelChoiceField(queryset=Land.objects.all(),
    widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Campana
        fields = ('recogida', 'kilos', 'dinero', 'campo')