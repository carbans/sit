from django.contrib import admin
from .models import Campana


class CampanaAdmin(admin.ModelAdmin):
    list_display = ('campo', 'kilos')

admin.site.register(Campana, CampanaAdmin)
