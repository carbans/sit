from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .models import Campana
from .forms import CampanaForm


@login_required()
def campana_add(request):
    """
    Formulario anadir campana
    """
    if request.method == 'POST':
        form = CampanaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('campo_lista')
    else:
        form = CampanaForm()

    return render(request, 'campana/campana_add.html', {'form': form})


@login_required()
def campana_delete(request, pk):
    query = Campana.objects.get(pk=pk)
    query.delete()

    return redirect('campo_lista')