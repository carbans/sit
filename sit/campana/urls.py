# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import campana_add, campana_delete


urlpatterns = [
    url(r'^add/$', campana_add, name='campana_add'),
    url(r'^delete/(?P<pk>[0-9]+)/$', campana_delete,
        name='campana_delete'),
]