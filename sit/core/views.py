from django.shortcuts import render
from django.db.models import Sum
from django.contrib.auth.decorators import login_required

from campo.models import Land
from agua.models import Agua


@login_required()
def index(request):
    contenido = {
        'notificaciones': '1',
        'mensaje': 'Hola mundo!',
        }
    campos = Land.objects.all()[:5]

    datosreal = []

    for mes in range(1, 12):
        cantidad = Agua.objects.filter(fecha__month=mes).aggregate(
            total=Sum('cantidad'))['total']
        if cantidad is None:
            cantidad = 0
        datosreal.append(cantidad)

    return render(request, 'index.html', {'datos': contenido,
                            'campos': campos, 'datosreal': datosreal})

