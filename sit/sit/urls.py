from django.conf.urls import url, include
from django.contrib import admin

#Import API
from rest_framework import routers
from campo.views import CampoViewSet

router = routers.DefaultRouter()
router.register(r'campos', CampoViewSet)

urlpatterns = [
    url(r'^', include('core.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^abono/', include('abono.urls')),
    url(r'^cuentas/', include('perfil.urls')),
    url(r'^campo/', include('campo.urls')),
    url(r'^agua/', include('agua.urls')),
    url(r'^campana/', include('campana.urls')),
    url(r'^api/', include(router.urls)),
    url(r'^api/auth/', include('rest_framework.urls',
        namespace='rest_framework')),
]