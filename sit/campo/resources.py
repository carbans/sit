from import_export import resources
from .models import Land


class CampoResource(resources.ModelResource):
    class Meta:
        model = Land
