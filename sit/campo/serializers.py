# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Land


class CampoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Land
        fields = ('id', 'nombre', 'alias', 'hectareas', 'poligono',
            'parcela', 'anoplantacion', 'numarboles', 'variedad')