from django.contrib import admin
from .models import Land


class CampoAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias', 'hectares')
    search_fields = ('name',)


admin.site.register(Land, CampoAdmin)
