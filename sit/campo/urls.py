# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import campo_add, CampoLista, campo_detalle
from .views import campo_export, campo_import
from .views import CampoDelete, CampoUpdate
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', campo_detalle, name='campo_detalle'),
    url(r'^add/$', campo_add, name='campo_add'),
    url(r'^lista/', CampoLista.as_view(), name='campo_lista'),
    url(r'^delete/(?P<pk>[0-9]+)/$', login_required(
        CampoDelete.as_view()), name='campo_delete'),
    url(r'^update/(?P<pk>[0-9]+)/$', login_required(
        CampoUpdate.as_view()), name='campo_update'),
    url(r'^export/$', campo_export, name='campo_export'),
    url(r'^import/$', campo_import, name='campo_import'),
]