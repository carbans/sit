from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models


class Land(models.Model):
    name = models.CharField(_('name'), max_length=200, unique=True)
    alias = models.CharField(_('alias'), max_length=100, null=True, blank=True)
    hectares = models.FloatField(_('hectares'), null=True, blank=True)
    polygon = models.CharField(_('polygon'), max_length=20, null=True, blank=True)
    plot = models.CharField(_('plot'), max_length=20, null=True, blank=True)
    plantation = models.DateField(_('date plantation'), null=True, blank=True)
    trees = models.IntegerField(_('number of trees'), null=True, blank=True)
    variety = models.CharField(_('variety'), max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.name

    def get_name(self):
        return self.name

    @classmethod
    def get_id(cls, uid):
        return Land.objects.get(pk=uid)

    class Meta:
        verbose_name = _('Land')
        ordering = ["name"]
