# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView

from .models import Land
from .resources import CampoResource
from .forms import CampoForm

from tablib import Dataset
#from agua.models import Agua, Dotacion
from agua.models import Agua
from campana.models import Campana

#Imports API
from .serializers import CampoSerializer
from rest_framework import viewsets


@login_required()
def campo_detalle(request, pk):
    campo = get_object_or_404(Land, pk=pk)
    aguas = Agua.objects.filter(campo=pk).order_by('fecha')
    campanas = Campana.objects.filter(campo=pk)

    totalagua = Agua.get_agua_mes(pk)

    #dot = Dotacion.get_dotacion_mes()
    #dotacion = dot.mcubicos * campo.hectareas

    """return render(request, 'campo/campo_detail.html', {'campo': campo,
        'aguas': aguas, 'campanas': campanas,
        'totalagua': totalagua, 'dotacion': dotacion})
        """
    return render(request, 'campo/campo_detail.html', {'campo': campo,
        'aguas': aguas, 'campanas': campanas,
        'totalagua': totalagua})


@login_required()
def campo_add(request):
    """
     Formulario anadir campo
    """
    if request.method == 'POST':
        form = CampoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('campo_lista')
    else:
        form = CampoForm()

    return render(request, 'campo/campo_add.html', {'form': form})


class CampoLista(ListView):
    model = Land
    template_name = 'campo/campo_list.html'
    queryset = Land.objects.order_by('pk')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampoLista, self).dispatch(*args, **kwargs)


class CampoDelete(DeleteView):
    model = Land
    template_name = 'campo/campo_confirm_delete.html'
    success_url = reverse_lazy('campo_lista')


class CampoUpdate(UpdateView):
    model = Land
    form_class = CampoForm
    template_name = 'campo/campo_update_form.html'
    success_url = reverse_lazy('campo_lista')


@login_required()
def campo_export():
    campo_resource = CampoResource()
    dataset = campo_resource.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="campos.csv"'

    return response


@login_required()
def campo_import(request):
    if request.method == 'POST':
        campo_resource = CampoResource()
        dataset = Dataset()
        nuevos_campos = request.FILES['campofile']

        #datos_importados = dataset.load(nuevos_campos.read())
        dataset.load(nuevos_campos.read())
        resultado = campo_resource.import_data(dataset, dry_run=True)

        if not resultado.has_errors():
            campo_resource.import_data(dataset, dry_run=False)

    return render(request, 'campo/campo_import.html')


class CampoViewSet(viewsets.ModelViewSet):
    queryset = Land.objects.all()
    serializer_class = CampoSerializer
