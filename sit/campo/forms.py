# -*- coding: utf-8 -*-
from django import forms
from .models import Land


class CampoForm(forms.ModelForm):
    nombre = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control',
                                                                          'placeholder': 'Ej. El boreguero...'}))
    alias = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control',
                                                                         'placeholder': 'Ej. La caseta...'}))
    hectareas = forms.FloatField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control',
                                                                                'placeholder': 'Ej. 25,5'}))
    poligono = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej. 18'}))
    parcela = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej. 518-519'}))
    anoplantacion = forms.DateField(required=True, widget=forms.TextInput(attrs={'class': 'form-control pull-rigth',
                                                                                 'id': 'datepicker',
                                                                                 'placeholder': 'Ej. 1/1/2000'}))
    numarboles = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control',
                                                                                   'placeholder': 'Ej. 520'}))
    variedad = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej. Clemenules'}))

    class Meta:
        model = Land
        fields = ('nombre', 'alias', 'hectareas', 'poligono', 'parcela', 'anoplantacion', 'numarboles', 'variedad')
