# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .views import signup, activate, account_activation_sent


urlpatterns = [
    url(r'^login/$', auth_views.login, {'template_name': 'perfil/login.html'},
        name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'login'},
        name='logout'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^confirmacion', account_activation_sent, name='cuenta_confirmacion'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),
]