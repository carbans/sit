from django.contrib import admin
from .models import Agua, Dotacion


class AguaAdmin(admin.ModelAdmin):
    list_display = ('campo', 'cantidad')


class DotacionAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'mcubicos')


admin.site.register(Agua, AguaAdmin)
admin.site.register(Dotacion, DotacionAdmin)