# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-12-29 10:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('campo', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agua',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField(verbose_name='cantidad')),
                ('fecha', models.DateField(blank=True, null=True, verbose_name='fecha')),
                ('campo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='campo.Land')),
            ],
        ),
        migrations.CreateModel(
            name='Dotacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(verbose_name='fecha dotacion')),
                ('mcubicos', models.IntegerField(verbose_name='metros cubicos')),
            ],
        ),
    ]
