from __future__ import unicode_literals
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.db.models import Sum

from campo.models import Land

from datetime import datetime


class Agua(models.Model):
    cantidad = models.IntegerField(_('cantidad'))
    fecha = models.DateField(_('fecha'), null=True, blank=True)
    campo = models.ForeignKey(to=Land, null=True, blank=True, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.campo.nombre

    @classmethod
    def get_agua_mes(cls, pk):
        fe = datetime.now()
        """totalagua = Agua.objects.filter(
            fecha__month=fe.month,
            fecha__year=fe.year,
            campo=pk).aggregate(total=Sum('cantidad'))['total']
        """
        primero = Agua.objects.filter(
            fecha__month=fe.month,
            fecha__year=fe.year,
            campo=pk).order_by('fecha')[0]

        ultimo = Agua.objects.filter(
            fecha__month=fe.month,
            fecha__year=fe.year,
            campo=pk).order_by('-fecha')[0]

        totalagua = ultimo.cantidad - primero.cantidad

        if totalagua is None:
            totalagua = 0

        return totalagua

    @classmethod
    def get_agua_total(cls, pk):
        totalagua = Agua.objects.filter(campo=pk).aggregate(
            total=Sum('cantidad'))['total']
        return totalagua


class Dotacion(models.Model):
    fecha = models.DateField(_('fecha dotacion'))
    mcubicos = models.IntegerField(_('metros cubicos'))

    def __unicode__(self):
        return self.fecha

    @classmethod
    def get_dotacion_mes(cls):
        fe = datetime.now()
        try:
            totaldotacion = Dotacion.objects.filter(fecha__month=fe.month, fecha__year=fe.year).get()
        except ObjectDoesNotExist:
            print("Either the entry of dotacion doesn't exist.")

        if totaldotacion is None:
            totaldotacion = 0

        return totaldotacion