# -*- coding: utf-8 -*-
from django import forms
from .models import Agua, Dotacion
from campo.models import Land


class AguaForm(forms.ModelForm):
    cantidad = forms.IntegerField(required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control',
            'placeholder': 'Ej. 150'}))

    fecha = forms.DateField(required=True,
    widget=forms.TextInput(attrs={'class': 'form-control pull-rigth',
    'id': 'datepicker', 'placeholder': 'Ej. 1/1/2017'}))
    campo = forms.ModelChoiceField(queryset=Land.objects.all(),
    widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Agua
        fields = ('cantidad', 'fecha', 'campo')


class DotacionForm(forms.ModelForm):
    mcubicos = forms.IntegerField(required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control',
            'placeholder': 'Ej. 60'}))

    fecha = forms.DateField(required=True,
    widget=forms.TextInput(attrs={'class': 'form-control pull-rigth',
    'id': 'datepicker', 'placeholder': 'Ej. 01/01/2017'}))

    class Meta:
        model = Dotacion
        fields = ('mcubicos', 'fecha')