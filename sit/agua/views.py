# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Agua, Dotacion
from .forms import AguaForm, DotacionForm


@login_required()
def agua_add(request):
    """
    Formulario anadir agua
    """
    if request.method == 'POST':
        form = AguaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('campo_lista')
    else:
        form = AguaForm()

    return render(request, 'agua/agua_add.html', {'form': form})


class AguaDelete(DeleteView):
    model = Agua
    template_name = 'agua/agua_confirm_delete.html'
    success_url = reverse_lazy('campo_lista')


@login_required()
def dotacion_add(request):
    """
    Formulario anadir dotacion
    """
    if request.method == 'POST':
        form = DotacionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = DotacionForm()

    return render(request, 'agua/dotacion/dotacion_add.html', {'form': form})


@login_required()
def dotacion_delete(request, pk):
    query = Dotacion.objects.get(pk=pk)
    query.delete()

    return redirect('dotacion_lista')


class DotacionLista(ListView):
    model = Dotacion
    template_name = 'agua/dotacion/dotacion_list.html'
    queryset = Dotacion.objects.order_by('pk')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DotacionLista, self).dispatch(*args, **kwargs)