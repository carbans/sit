# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from .views import agua_add, AguaDelete, dotacion_add, DotacionLista
from .views import dotacion_delete


urlpatterns = [
    url(r'^add/$', agua_add, name='agua_add'),
    url(r'^delete/(?P<pk>[0-9]+)/$', login_required(AguaDelete.as_view()),
        name='agua_delete'),
    url(r'^dotacion/add/$', dotacion_add, name='dotacion_add'),
    url(r'^dotacion/delete/(?P<pk>[0-9]+)/$', dotacion_delete,
        name='dotacion_delete'),
    url(r'^dotacion/lista/$', DotacionLista.as_view(), name='dotacion_lista'),
]