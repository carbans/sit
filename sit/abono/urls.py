# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import abono_add, AbonoLista, AbonoDelete
from .views import equivalencia_add, EquivalenciaLista, EquivalenciaDelete
from .views import plan_add, PlanLista, PlanDelete

urlpatterns = [
    url(r'^add/$', abono_add, name='abono_add'),
    url(r'^delete/(?P<pk>[0-9]+)/$', AbonoDelete.as_view(),
    name='abono_delete'),
    url(r'^lista/$', AbonoLista.as_view(), name='abono_lista'),
    url(r'^equivalencia/add/$', equivalencia_add, name='equivalencia_add'),
    url(r'^equivalencia/delete/(?P<pk>[0-9]+)/$', EquivalenciaDelete.as_view(),
    name='equivalencia_delete'),
    url(r'^equivalencia/lista/$', EquivalenciaLista.as_view(),
    name='equivalencia_lista'),
    url(r'^plan/add/$', plan_add, name='plan_add'),
    url(r'^plan/delete/(?P<pk>[0-9]+)/$', PlanDelete.as_view(),
    name='plan_delete'),
    url(r'^plan/lista/$', PlanLista.as_view(), name='plan_lista'),
]
