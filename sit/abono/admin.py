from django.contrib import admin
from abono.models import Equivalencia, Abono, Plan


class EquivalenciaAdmin(admin.ModelAdmin):
    list_display = ('nombre',)


class AbonoAdmin(admin.ModelAdmin):
    list_display = ('nombre',)


class PlanAdmin(admin.ModelAdmin):
    list_display = ('abono', 'fecha', 'campo', 'kilos')


admin.site.register(Equivalencia, EquivalenciaAdmin)
admin.site.register(Abono, AbonoAdmin)
admin.site.register(Plan, PlanAdmin)