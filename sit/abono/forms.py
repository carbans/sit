from django import forms
from .models import Abono, Plan
from campo.models import Land


class AbonoForm(forms.ModelForm):
    nombre = forms.CharField(required=True,
        widget=forms.TextInput(attrs={'class': 'form-control',
            'placeholder': 'Ej. Nitrato Amonico...'}))
    nitrogenori = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 0,33'}))
    fosforori = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 0,12'}))
    potasiori = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 0,33'}))
    azufreri = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 0,33'}))
    magnesiori = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 0,33'}))

    class Meta:
        model = Abono
        fields = ('nombre', 'nitrogenori', 'fosforori', 'potasiori',
            'azufreri', 'magnesiori')


class EquivalenciaForm(forms.ModelForm):
    nombre = forms.CharField(required=True,
         widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder': 'Ej. Nitrato de cal...'}))
    nitrogenok = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 2,2'}))
    fosforok = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 3,2'}))
    potasiok = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 3,5'}))
    azufrek = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 3,3'}))
    magnesiok = forms.FloatField(required=True,
        widget = forms.NumberInput(attrs={'class': 'form-control pull-right',
            'placeholder': 'Ej. 1,3'}))

    class Meta:
        model = Abono
        fields = ('nombre', 'nitrogenok', 'fosforok', 'potasiok', 'azufrek',
             'magnesiok')


class PlanForm(forms.ModelForm):
    abono = forms.ModelChoiceField(queryset=Abono.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}))
    fecha = forms.DateField(required=True,
        widget=forms.TextInput(attrs={'class': 'form-control pull-rigth',
        'id': 'datepicker', 'placeholder': 'Ej. 01/01/2017'}))
    kilos = forms.FloatField(required=True,
        widget = forms.NumberInput( attrs = {'class': 'form-control pull-right',
            'placeholder': 'Ej. 2,1'}))
    campo = forms.ModelChoiceField(queryset=Land.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Plan
        fields = ('abono', 'fecha', 'kilos', 'campo')

