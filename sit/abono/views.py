# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
#from django.http import Http404, HttpResponseRedirect
from django.views.generic import ListView
from django.views.generic.edit import DeleteView
#from django.views.generic.detail import DetailView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from abono.models import Abono, Equivalencia, Plan
from abono.forms import AbonoForm, EquivalenciaForm, PlanForm


@login_required()
def abono_add(request):
    """
     Formulario anadir Abono
    """
    if request.method == 'POST':
        form = AbonoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('abono_lista')
    else:
        form = AbonoForm()

    return render(request, 'abono/abono_add.html', {'form': form})


class AbonoLista(ListView):
    model = Abono
    template_name = 'abono/abono_list.html'
    queryset = Abono.objects.order_by('pk')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AbonoLista, self).dispatch(*args, **kwargs)


class AbonoDelete(DeleteView):
    model = Abono
    template_name = 'abono/abono_confirm_delete.html'
    success_url = reverse_lazy('abono_lista')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AbonoDelete, self).dispatch(*args, **kwargs)


@login_required()
def equivalencia_add(request):
    """
     Formulario anadir Equivalente
    """
    if request.method == 'POST':
        form = EquivalenciaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('equivalencia_lista')
    else:
        form = EquivalenciaForm()

    return render(request, 'abono/equivalencia/equivalencia_add.html',
         {'form': form})


class EquivalenciaLista(ListView):
    model = Equivalencia
    template_name = 'abono/equivalencia/equivalencia_list.html'
    queryset = Equivalencia.objects.order_by('pk')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EquivalenciaLista, self).dispatch(*args, **kwargs)


class EquivalenciaDelete(DeleteView):
    model = Equivalencia
    template_name = 'abono/equivalencia/equivalencia_confirm_delete.html'
    success_url = reverse_lazy('equivalencia_lista')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EquivalenciaDelete, self).dispatch(*args, **kwargs)


@login_required()
def plan_add(request):
    """
     Formulario anadir Plan
    """
    if request.method == 'POST':
        form = PlanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('plan_lista')
    else:
        form = PlanForm()

    return render(request, 'abono/plan/plan_add.html', {'form': form})


class PlanLista(ListView):
    model = Plan
    template_name = 'abono/plan/plan_list.html'
    queryset = Plan.objects.order_by('pk')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PlanLista, self).dispatch(*args, **kwargs)


class PlanDelete(DeleteView):
    model = Plan
    template_name = 'abono/plan/plan_confirm_delete.html'
    success_url = reverse_lazy('plan_lista')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PlanDelete, self).dispatch(*args, **kwargs)