from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from campo.models import Land


class Equivalencia(models.Model):
    nombre = models.CharField(_('nombre'), max_length=100)
    nitrogenok = models.FloatField(_('kilos nitrogeno'))
    fosforok = models.FloatField(_('kilos potasio'))
    potasiok = models.FloatField(_('kilos fosforo'))
    azufrek = models.FloatField(_('kilos azufre'))
    magnesiok = models.FloatField(_('kilos magnesio'))

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre

    class Meta:
        ordering = ["id"]


class Abono(models.Model):
    nombre = models.CharField(_('nombre'), max_length=100)
    nitrogenori = models.FloatField(_('riqueza nitrogeno'))
    fosforori = models.FloatField(_('riqueza fosforo'))
    potasiori = models.FloatField(_('riqueza potasio'))
    azufreri = models.FloatField(_('riqueza azufre'))
    magnesiori = models.FloatField(_('riqueza magnesio'))

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre

    class Meta:
        ordering = ["id"]


class Plan(models.Model):
    abono = models.ForeignKey(to=Abono, null=True,
    blank=True, on_delete=models.CASCADE)
    fecha = models.DateField(_('fecha'))
    kilos = models.FloatField(_('kilos'))
    campo = models.ForeignKey(to=Land, null=True,
    blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.abono.nombre

    def __unicode__(self):
        return self.abono.nombre

    class Meta:
        ordering = ["id"]
        verbose_name = _('Plan')
        verbose_name_plural = _('Planes')
