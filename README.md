# SIT
SIT is a acronym of System Irrigate Thing, is a project of automation of irrigate. This repository is a web interface for interact with the electronic part of the system.

The actual version is **Version 0.5**

## Install
We install the next packages:
```bash
sudo apt update
sudo apt install python-pip python-dev libpq-dev postgresql postgresql-contrib
```

## Dev Enviroment
We make a virtual environment:
```bash
sudo pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requeriments.txt
```

Then we launch the migrates scripts of Django
```bash
./manage.py makemigrations abono
./manage.py makemigrations campo
./manage.py migrate
``` 
Finally we launch the server
```bash
./manage.py runserver
```


-----------------------------------------------------------
# SIT
SIT es el acronimo de System Irrigate Thing, es un proyecto para la automatización del riego. Esta es la interfaz web para interactuar con el sistema.

**Versión 0.5**

## Instalación
Instalamos los siguientes paquetes:
```bash
sudo apt update
sudo apt install python-pip python-dev libpq-dev postgresql postgresql-contrib nginx
```

Creamos el entorno virtual que contendra nuestra aplicación
```bash
sudo pip install virtualenv
```


